server:
  - !channel
    port: 50052
    insecure-mode: true

description: >
  Docker Compose storage configuration:
    - Unauthenticated plain HTTP at :50052
    - Single instance: [unnamed]
    - On-disk data stored in /var
    - Hosted services:
       - ActionCache
       - ByteStream
       - ContentAddressableStorage
       - ReferenceStorage

authorization:
  method: none

monitoring:
  enabled: false

instances:
  - name: ''
    description: |
      The unique unnamed instance.

    storages:
      - !disk-storage &data-store
        path: /var/lib/buildgrid/store

    services:
      - !action-cache
        storage: *data-store
        max-cached-refs: 1024
        cache-failed-actions: true
        allow-updates: true

      - !cas
        storage: *data-store

      - !bytestream
        storage: *data-store

      - !reference-cache
        storage: *data-store
        max-cached-refs: 512
        allow-updates: true

thread-pool-size: 100
